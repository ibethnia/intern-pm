///<reference types="cypress" />
import 'cypress-file-upload';

const baseUrl= "http://app.privy.id";
const fileName="tes.pdf";

before(()=>{
    cy.visit(baseUrl);
    cy.fixture('credential').then((data)=>{
        cy.get('input[name="user[privyId]"]').type(data.privyID);
        cy.contains("CONTINUE").click();
        cy.get('input[name="user[secret]"]',{timeout: 5000}).type(data.password);
        cy.contains("CONTINUE").click();
    })
})

describe('Sign and Share Automation',()=>{

it('Automated', ()=>{
           cy.viewport(1280, 720);
   
            cy.contains("Welcome Back");
            cy.get('button[id="v-step-0"]',{timeout:10000}).click();
            cy.get('#upload-modal___BV_modal_body_ > div > div > div:nth-child(2) > a',{timeout:5000}).click();
            cy.wait(2000);

            //Upload the File
            cy.get('input[type="file"]').attachFile(fileName,{ subjectType: 'drag-n-drop' });
            cy.get("footer > button.btn.btn-danger").click();
            cy.get('button[id="step-document-1"]',{timeout:60000}).click();

            cy.get('#__layout > div > div > div.layout-default__container > div > div > div.workflow__content > div > div > div > div.workflow-sign__navigation.pdf-control > button.btn.btn-success.mx-2.my-2').click();
            cy.wait(2000)
            cy.contains('Send via QR Code').click();
            cy.contains('Send OTP').click();
            cy.get('[id=v-recipient-1]',{timeout:60000}).click();
            cy.get('div.multiselect__tags').type('GA4738');
            cy.wait(7000);
            cy.get('ul.multiselect__content').click();
            cy.get('#__BVID__234').first().check({force:true, timeout:10000});
            cy.get('.form-role > div:nth-child(2)').click();
            cy.wait(5000);
           
            cy.get('.form-role > div:nth-child(3) > ul:nth-child(1) > li:nth-child(1) > span:nth-child(1)',{timeout:10000}).click();
            cy.get('button.pr-4').click({timeout:10000});
            cy.contains('Continue',{timeout:10000}).click();
            

        })

});
