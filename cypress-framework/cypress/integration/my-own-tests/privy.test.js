/// <reference types="cypress" />

describe('basic test privy', () => {
    it('Self Sign', () => {
        cy.viewport(1280, 720);
        cy.visit('app.privy.id');
        cy.get('[id=__BVID__4]').type('XV1807');
        cy.contains('CONTINUE').click();
        cy.get('[id=__BVID__6]').type('Yeskialtefa18');
        cy.contains('CONTINUE').click();
        cy.get('[id=v-step-0]').click();
        cy.contains('Self Sign').click();
        cy.wait(2000);
        cy.get('input[type="file"]').attachFile('test.pdf',{ subjectType: 'drag-n-drop' });
        cy.get('button.btn:nth-child(2)',{timeout:5000}).click();
        cy.get('button[id="step-document-1"]',{timeout:20000}).click();
        cy.contains('Done').click();
        cy.wait(4000);
        cy.contains('Send via QR Code').click();
        cy.contains('Send OTP').click();
    });

    it.only('Sign and share', () => {
        cy.viewport(1280, 720);
        cy.visit('app.privy.id');
        cy.get('[id=__BVID__4]').type('XV1807');
        cy.contains('CONTINUE').click();
        cy.get('[id=__BVID__6]').type('Yeskialtefa18');
        cy.contains('CONTINUE').click();
        cy.get('[id=v-step-0]').click();
        cy.contains('Sign & Request').click();
        cy.wait(2000);
        cy.get('input[type="file"]').attachFile('test.pdf',{ subjectType: 'drag-n-drop' });
        cy.get('button.btn:nth-child(2)').click();
        cy.get('button[id="step-document-1"]',{timeout:20000}).click();
        cy.contains('Done').click();
        cy.wait(4000);
        cy.contains('Send via QR Code').click();
        cy.contains('Send OTP').click();
        cy.wait(20000);
        cy.get('[id=v-recipient-1]').click();
        cy.get('div.multiselect__tags').type('GYTI4E');
        cy.wait(7000);
        cy.get('ul.multiselect__content').click();
        cy.wait(10000);
        cy.get('#__BVID__233 > div:nth-child(1)').check({force:true, timeout:10000});
        cy.get('#__BVID__220 > div > div > div.multiselect__tags').click();
        cy.wait(5000);
        cy.get('#__BVID__220 > div > div > div.multiselect__content-wrapper > ul > li:nth-child(1) > span').click({timeout:10000});
        cy.get('#__layout > div > div > div.layout-default__container > div > div > div.workflow__content > div > div > div > div:nth-child(1) > div > span > div > div.workflow-share__form > div > span > div.mt-4.button-edit > div:nth-child(2) > button').click()
        cy.get('#__BVID__179 > div > div > div > div.mt-3.text-right > button').click({timeout:10000})
    });

    it('Change Image Signature', () => {
        cy.viewport(1280, 720);
        cy.visit('app.privy.id');
        cy.get('[id=__BVID__4]').type('XV1807');
        cy.contains('CONTINUE').click();
        cy.get('[id=__BVID__6]').type('Yeskialtefa18');
        cy.contains('CONTINUE').click();
        cy.contains('Change My Signature Image').click();
        cy.contains('Add Signature').click();
        cy.get('input#name-initial.form-control.is-invalid').clear().type('Fania');
        cy.contains('Save').click();
    });
});
